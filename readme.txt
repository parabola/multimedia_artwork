This repository contains most of the Parabola artwork from the mailing
list and around the web that I could find.

Notably/intentionally not included are:
 * The artwork uploaded to bugs.parabolagnulinux.org, specifically
   <https://bugs.parabolagnulinux.org/bugs/issue196>, as that service
   is no longer online. There are backups of the data though, but I
   have not yet figured out how to read it.

Each branch of the repository is a collection of artwork.  The
official artwork is (obviously) contained in the `official/*`
branches, unofficial ("contributed") artwork is in `contrib/$USER`
branches.

On each branch, running `make` should extract any archives, or create
any derived files.

The `readme.txt` file in each branch should identify the author and
license.

Perhaps one day I'll make it so that wiki users or something can push
to `contrib/$USER` branches without having to get special access.
